import React, { useEffect, useState} from 'react';
import $ from 'jquery';
import './WeeklyCalendar.css';
import {
    fillWeekDays,
    weekID,
    addTimeDataToElement,
    disableDatesBeforeToday,
    addActiveElements
} from "./useWeeklyCalendar.js";



export function WeeklyCalendar(props) {
    // Alldate keeps each week with unique key (year + week number) with weekID() function.
    // Inside that it keeps checkbox value for get next 4 weeks and all the selected dates.
    const [allDate, setAllDate] = useState({
        [weekID()]: {
            checkbox: false,
            dates: [],
        }
    });

    // useEffect runs when component is mounted
    useEffect(() => {

        if (!props.isTeacher) {
            $('.checkbox').css('display','none');
        }

        // Fills first row with date, month and week name.
        fillWeekDays();
        // Adds date attribute(month,day and hour) to each pickable cells in the table.
        addTimeDataToElement();
        // Checks if the cells are not in past. If it's adds 'disabled' class to cells, so it's not clickable.
        disableDatesBeforeToday(props.isTeacher, props.datesDict);
    }, [])


    // Buttons click handler for changing week forward and backward
    const onClickHandlerWeekChange = value => {

        // Fills first row with date, month and week name.
        fillWeekDays(value);
        // Adds date attribute(month,day and hour) to each pickable cells in the table.
        addTimeDataToElement();
        // Checks if the cells are not in past. If it's adds 'disabled' class to cells, so it's not clickable.
        disableDatesBeforeToday(props.isTeacher, props.datesDict);

        // If user choose some dates and goes to next week, it removes all previous active classes
        // to show user blank week.
        for (let item of $(".active")) {
            $(item).removeClass("active");
        }

        // If user choose dates in week and revisits that week again, it iterates through state
        // and if user picked any dates from that week, adds active class.
        let weekIDkeys = Object.keys(allDate);
        for (let key of weekIDkeys) {
            if (key === weekID()) {
                for (let el of allDate[key].dates) {
                    const activeElement = $(`[date='${el}']`);
                    if (!activeElement.hasClass("disabled")) {
                        activeElement.addClass("active");
                    }
                }
            }
        }

        // Sets new dates and checkbox condition to object which has current week id.
        setAllDate(prevState => {

            // If user visits the week for the first time, checkbox is false,
            // and if user visits and change checkbox value, I get that value from previous State
            let checkboxVal;

            if (!prevState[weekID()]) {
                checkboxVal = false;
            } else {
                checkboxVal = prevState[weekID()].checkbox;
            }
            return {
                ...prevState,
                [weekID()]: {
                    checkbox: checkboxVal,
                    dates: [...addActiveElements()]
                }
            };
        });
    };


    // Triggers when user click any cell that is pickable
    const onClickHandlerCalendar = e => {
        const { target } = e;

        // It explain itself :)
        if (target.classList.contains("pickable") && !target.classList.contains("disabled")) {
            $(target).toggleClass("active");
        }

        // Setting state on every click from user 
        setAllDate(prevState => {
            let checkboxVal;
            if (!prevState[weekID()]) {
                checkboxVal = false;
            } else {
                checkboxVal = prevState[weekID()].checkbox;
            }
            return {
                ...prevState,
                [weekID()]: {
                    checkbox: checkboxVal,
                    dates: [...addActiveElements()]
                }
            };
        });
    };


    const onChangeHandlerCheckBox = () => {

        // Updates state when checkbox is clicked
        setAllDate(prevState => {
            return {
                ...prevState,
                [weekID()] : {
                    checkbox: !prevState[weekID()].checkbox,
                    dates: [...addActiveElements()]
                }
            }
        })
    }

    // It runs through allDate object and checks object with unique key if checkbox is true or not. 
    // If it's true adds next four weeks to array and destruct that array in pickedDate state.

    const getPickedDays = () => {
        let daysWithNoCheckboxTrue = [];
        let newPickedDates = [];
        let pickedDictionary = {}
        let objKeys = Object.keys(allDate);

        for (let stateWeekID of objKeys) {
            const prop = allDate[stateWeekID];

            for (let dateItem of prop.dates) {
                const itemYear = weekID().slice(0, 4);
                const itemHour = dateItem.split("-")[0];
                const itemDay = dateItem.split("-")[1].split(".")[0];
                const itemMonth = dateItem.split("-")[1].split(".")[1];
                let dateArr = [];

                if (props.isTeacher) {

                    if (prop.checkbox === true) {
                        let addedWeekDate = new Date(itemYear,itemMonth - 1,itemDay);

                        let local = "en-US";
                        let options = {
                            day: "numeric",
                            month: "numeric"
                        };

                        for (let i = 0; i < 4; i++) {
                            addedWeekDate.setDate(addedWeekDate.getDate() + 7);
                            dateArr.push(addedWeekDate.toLocaleDateString(local, options));
                        }

                        const pickedDate = itemDay + "." + itemMonth + " " + itemHour;

                        for (let item of dateArr) {
                            const slicedItem = item.split("/");
                            let day = slicedItem[1];
                            let month = slicedItem[0];

                            const next4Week = day + "." + month + " " + itemHour;

                            newPickedDates.push(next4Week, pickedDate);
                        }
                    } else {
                        daysWithNoCheckboxTrue.push(dateItem.split("-")[1] + " " + dateItem.split("-")[0]);

                        newPickedDates.push(...daysWithNoCheckboxTrue);
                    }

                } else {
                    let pickedDates = new Date(`${itemYear} ${itemMonth} ${itemDay} ${itemHour}`)

                    for (let key of Object.keys(props.datesDict)) {
                        const dictDate = new Date(key)

                        if (pickedDates.getTime() === dictDate.getTime()) {
                            pickedDictionary[`${dictDate.getDate()}.${dictDate.getMonth() + 1} ${dictDate.getHours()}:00`] = props.datesDict[key]
                        }
                    }
                }
            }
        }

        if (props.isTeacher) {
            const uniqueDates = new Set(newPickedDates);
            props.confirmHandler(Array.from(uniqueDates));
        } else {
            props.confirmHandler(pickedDictionary);
        }
    };

    const onClickHandlerSubmit = () => {
        getPickedDays();
    }

    return (
        <div className="calendar-component">
            <div className="container">
                <div className="calendar-header">
                    <div className="btn left" onClick={() => onClickHandlerWeekChange('goLeft')}><i className="fas fa-chevron-left"></i></div>
                    <h1 className="calendar-header--title">{""}</h1>
                    <div className="btn right" onClick={() => onClickHandlerWeekChange('goRight')}><i className="fas fa-chevron-right"></i></div>
                </div>

                <table className="calendar-table" onClick={e => onClickHandlerCalendar(e)}>
                    <thead className='thead'>
                        <tr className="week-days">
                            <th>שבת</th>
                            <th>שישי</th>
                            <th>חמישי</th>
                            <th>רביעי</th>
                            <th>שלישי</th>
                            <th>שני</th>
                            <th>ראשון</th>
                            <th></th>
                        </tr>
                        <tr className='0-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">08:00</th>
                        </tr>
                        <tr className='1-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">09:00</th>
                        </tr>
                        <tr className='2-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">10:00</th>
                        </tr>
                        <tr className='3-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">11:00</th>
                        </tr>
                        <tr className='4-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">12:00</th>
                        </tr>
                        <tr className='5-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">13:00</th>
                        </tr>
                        <tr className='6-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">14:00</th>
                        </tr>
                        <tr className='7-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">15:00</th>
                        </tr>
                        <tr className='8-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">16:00</th>
                        </tr>
                        <tr className='9-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">17:00</th>
                        </tr>
                        <tr className='10-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">18:00</th>
                        </tr>
                        <tr className='11-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">19:00</th>
                        </tr>
                        <tr className='12-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">20:00</th>
                        </tr>
                        <tr className='13-row'>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="pickable"></th>
                            <th className="time">21:00</th>
                        </tr>
                        <tr className='14-row'>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="pickable"></th>
                        <th className="time">22:00</th>
                    </tr>
                    </thead>
                </table>

                <label className="checkbox">
                    <input 
                        type="checkbox" 
                        name="check" 
                        className="checkbox-input"
                        date={weekID()}
                        checked={allDate[weekID()].checkbox}
                        onChange={onChangeHandlerCheckBox}
                    />
                    <span className="checkmark"></span>
                    <span>בחר 4 שבועות קדימה'</span>
                </label>

                <button className="confirm btn" onClick={onClickHandlerSubmit}>Confirm</button>
            </div>
        </div>
    )
}
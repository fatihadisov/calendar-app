import React, { useState } from 'react';
import './App.css';
import { WeeklyCalendar } from './components';

function App() {
  const myConfirmHandler = (value) => {
    console.log(value);
  }

  const [isTeacher, setIsTeacher] = useState(false);


  const datesDict = {
    [new Date('2020 04 18 12:00')] : 'd1_value',
    [new Date('2020 04 13 13:00')] : 'd2_value',
    [new Date('2020 04 15 18:00')] : 'd3_value',
  }

  return (
    <div className="App">
      <WeeklyCalendar
        isTeacher={isTeacher} 
        datesDict={datesDict}
        confirmHandler={(value) => myConfirmHandler(value)}
      />
    </div>
  );
}

export default App;
